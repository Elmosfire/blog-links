constant genotype : Type
constant color: Type

structure Gene (α : Type) :=
mk :: (l : α) (r : α)

constant phenotype: Gene genotype -> color
constant child_pheno: color -> color -> color
constant viable: Gene genotype -> Prop

constant child_geno: Gene genotype -> Gene genotype -> Gene genotype

axiom inherit {x y: Gene genotype}: child_geno x y = Gene.mk x.l y.r

axiom consistent {x y: Gene genotype}: viable x ∧  viable y ∧ viable (child_geno x y)  -> phenotype (child_geno x y) = child_pheno (phenotype x) (phenotype y)

axiom symmetry {x y: color}: child_pheno x y = child_pheno y x
axiom pair_unique {x: Gene genotype}: {Gene. l:= x.l, r:= x.r}=x

#print inherit
#print consistent
#print symmetry


lemma refl_ {x: Gene genotype}: viable x -> phenotype (child_geno x x) = phenotype x :=
begin
  have refl0: child_geno x x = x,
  rw inherit,
  apply pair_unique,
  rw refl0,
  assume p,
  refl,
end

theorem final {x y: Gene genotype}: viable x ∧ viable y ∧ viable (child_geno x y)  -> phenotype x =  phenotype y :=
begin
let z := child_geno x y,
have zdef: z = child_geno x y,
refl,
rw inherit at zdef,
have leftbot: z = child_geno x z,
rw inherit,
rw zdef,
have lefttop: x = child_geno z x,
rw inherit,
rw zdef,
simp,
rw pair_unique,
have rightbot: z = child_geno z y,
rw inherit,
rw zdef,
have righttop: y = child_geno y z,
rw inherit,
rw zdef,
simp,
rw pair_unique,
assume p,
cases p with px pyz,
cases pyz with py pz,
rw inherit at pz,
rw <-zdef at pz,
have left_: phenotype x = phenotype z,
rw leftbot,
have h: phenotype x = phenotype(child_geno z x ),
rw <-lefttop,
rw h,
rw consistent,
rw symmetry,
rw <-consistent,
apply and.intro,
apply px,
apply and.intro,
apply pz,
rw <-leftbot,
apply pz,
apply and.intro,
apply pz,
apply and.intro,
apply px,
rw <-lefttop,
apply px,
have right_: phenotype y = phenotype z,
rw rightbot,
have h: phenotype y = phenotype(child_geno y z ),
rw <-righttop,
rw h,
rw consistent,
rw symmetry,
rw <-consistent,
apply and.intro,
apply pz,
apply and.intro,
apply py,
rw <-rightbot,
apply pz,
apply and.intro,
apply py,
apply and.intro,
apply pz,
rw <-righttop,
apply py,
rw left_,
rw right_
end

#print final
